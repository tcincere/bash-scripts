#!/usr/bin/env bash

# Define paths

# Docker compose paths
OLLAMA_YML="$HOME/Documents/docker/ollama/docker-compose.yml"
OPENWEBUI_YML="$HOME/Documents/docker/openwebui/docker-compose.yml"

# URL for openwebui
URL="http://127.0.0.1:3000"

# Folder paths
FAVICON_DIR="$HOME/Pictures/favicons"
ICON_OLLAMA="$FAVICON_DIR/ollama.jpg"
ICON_OPENWEBUI="$FAVICON_DIR/openwebui.jpg"

# Favicon download links
ICON_DOWNLOAD_OLLAMA="https://favicon.twenty.com/ollama.com"
ICON_DOWNLOAD_OPENWEBUI="https://favicon.twenty.com/docs.openwebui.com"

# Function to make needed directories
make_dirs() {

    # Make favicon directory if not exists already
    mkdir -p "$FAVICON_DIR"

    # If dir exists check if favicons exist 
    if [ -d "$FAVICON_DIR" ]; then

        # Download favicon if doesn't existing using vars
        if ! [ -f "$ICON_OLLAMA" ]; then
            wget "$ICON_DOWNLOAD_OLLAMA" -O "$ICON_OLLAMA" &>/dev/null
        fi

        if ! [ -f "$ICON_OPENWEBUI" ]; then
            wget "$ICON_DOWNLOAD_OPENWEBUI" -O "$ICON_OPENWEBUI" &>/dev/null
        fi
    fi

}

# Call function
make_dirs

# Template for notify-send
NOTIFY_OLLAMA="notify-send -i ${ICON_OLLAMA}"
NOTIFY_OPENWEBUI="notify-send -i ${ICON_OPENWEBUI}"

# Start the first Docker container and check if it succeeds
if docker compose -f "$OLLAMA_YML" up -d &>/dev/null; then
    $NOTIFY_OLLAMA "Ollama service started successfully."
else
    $NOTIFY_OLLAMA "Error starting Ollama service!"
    exit 1
fi

# Start the second Docker container and check if it succeeds
if docker compose -f "$OPENWEBUI_YML" up -d &>/dev/null; then
    $NOTIFY_OPENWEBUI "OpenWebUI service started successfully."
else
    $NOTIFY_OPENWEBUI "Error starting OpenWebUI service!"
    exit 1
fi


# Function to check if local set (openwebui) is reachable
is_reachable() {
  curl --silent --head --fail $1 > /dev/null
}

while ! is_reachable "$URL"; do
    sleep 2
done

$NOTIFY_OPENWEBUI "Service reachable"
$(which flatpak) run io.github.zen_browser.zen "$URL" &>/dev/null

