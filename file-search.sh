#!/bin/zsh

# Define the directory to search
directory="$USER_DIR"
FZF_OPTIONS=(-i --height=40% \
--pointer='>>'
--prompt="> "
--color=16 --reverse 
--info=hidden 
--color=fg:242,fg+:#ffcb1c,hl:#b16286,hl+:#4cb550,border:#dedddd 
--header="" 
--border=rounded)

# Variables to store long lists of extensions and directories to exclude.
USER_DIR="/home/$(whoami)/"
GREP_EXCLUDE_DIRS="cursors|peazip|jan"
FD_EXTENSIONS='*.{sh,rs,py,c,php,md,mp4,mov,avi,mkv,mpeg,jpg,jpeg,png,pdf,conf,rasi,txt,toml}'
# echo $USER_DIR

# Find files with specified extensions.
file="$(fd -t f --base-directory="$USER_DIR" --glob "$FD_EXTENSIONS")"

# Find plaintext/configuration files without extensions.
file_no_ext=$(fd '^[^.]*$' --type f --base-directory="$USER_DIR" | grep -viE "$GREP_EXCLUDE_DIRS")

# Add each variable to create a single list and remove all but the last element, sort and parse through fzf.
files="$(echo ${file}${file_no_ext} | awk -F '/' '{print $NF}' | sort -ru | fzf "${FZF_OPTIONS[@]}")"

# If not empty
if [ -n "$files" ]; then

  # Find absolute path of file and some files may have same name, in different location.
  # file_location=$(fd "$files" --base-directory="$USER_DIR")

  # Find all files which match pattern and store in array.
  # Uses zsh array
  file_location=("${(@f)$(fd "$files" --base-directory="$USER_DIR")}")

  file_number="${#file_location[@]}"

  # If number of lines exceed 1, then allow user to choose which file to open.
  # Here, if only one file located, choosing file becomes redundant.
  if [ "$file_number" -gt 1 ]; then
    file_location=$(fd "$files" --base-directory="$USER_DIR" | fzf "${FZF_OPTIONS[@]}" | sed 's|^./||')
  fi

  # Remove starting characters './' which fd provides.

  full_path="${USER_DIR}${file_location}"
  # echo $full_path

  # Check mime type of file.
  mime_type=$(file -b --mime-type "$full_path")
    
  # Case statement to open file in appropriate program
  case "$mime_type" in

    *"text"*)
        /usr/bin/hx "$full_path"
        # helix "$full_path"
        ;;
    *"pdf"*)
        # /usr/bin/zathura "$full_path" &>/dev/null &
        /usr/bin/evince "$full_path" &>/dev/null &
        ;;
    *"image"*)
        /usr/bin/loupe "$full_path" &>/dev/null &
        ;;
    *"video"*)
        # vlc "$full_path" &>/dev/null &
        /usr/bin/celluloid "$full_path" &>/dev/null &
        ;;
    *)
       # Handle other cases.
       echo "Filetype not supported!"
       exit 1
       ;;
  esac

else
  exit 1
fi
    
