#!/usr/bin/env zsh

link="$1"

# Options for fzf
FZF_OPTIONS=(-i -m \
--height=40% 
--pointer='>>'
--prompt="> "
--color=16 --reverse 
--info=hidden 
--color=fg:242,fg+:#ffcb1c,hl:#b16286,hl+:#4cb550,border:#dedddd 
--header="" 
--border=rounded)

# Constants
GREP_IGNORE="downloader.sh|"


# Status
status_download="[STATUS] Video downloaded!"
status_remove="[STATUS] Removed files!"
status_remove="[STATUS] Skipped removal."
status_error="[ERROR] Link missing!"
status_time="[ERROR] Start and/or end time missing!"

# Headers
h_main="gum style --border double  --margin '1' --padding '1 2' --border-foreground 212 'Video downloader'"

t_cutter="gum style --bold  'Cutter' --padding '1 2' --italic --underline --foreground='212'"
t_downloader="gum style --bold  'Downloader' --padding '1 2' --italic --underline --foreground='212'"
t_encryption="gum style --bold  'Encryption' --padding '1 2' --italic --underline --foreground='212'"
t_decryption="gum style --bold  'Decryption' --padding '1 2' --italic --underline --foreground='212'"
t_remove="gum style --bold  'Remove' --padding '1 2' --italic --underline --foreground='212'"
t_move="gum style --bold  'Move videos to another directory' --padding '1 2' --italic --underline --foreground='212'"

# Global variables
filename=""

main() {

  # Header 
  eval "$h_main"


  if [ "$link" ] && ! [ -f "$filename" ]; then
    filename=$(gum input --placeholder="video.mp4" --header="Filename")
    dl_video
  else

    OPTIONS=$(gum choose --item.foreground 250 "Download" "Cut" "Encrypt" "Decrypt" "Move" "Remove" "Quit")

    case $OPTIONS in

      Download)
        dl_video
        ;;

      Cut)
        cut_video
        ;;
      Encrypt)
        encrypt_cuts
        ;;
      Decrypt)
        decrypt_cuts
        ;;
      Remove)
        securely_remove_files
        ;;
      Move)
        move_files
        ;;
      Quit)
        exit 0
        ;;
      *)
        exit 1
        ;;
    esac

  fi
  
}

dl_video() {

  if [ -n "$link" ]; then

    # Header 
    eval "$t_downloader"

    gum spin --title "Downloading video (${filename})..." -- yt-dlp -S 'res:720' --external-downloader axel --external-downloader-args '-n 15 -a' "$link" -o "$filename" || exit 1 


    if [ -f "$filename" ]; then
      reply=$(gum choose --item.foreground 250 "Cut" "Don't cut")

      if [ "$reply" = "Cut" ]; then
        cut_video
      else
        main
      fi  
    fi

  else

    printf "%s\n" "$status_error"
    exit 1

  fi

}

cut_video() {

  # Header
  eval "$t_cutter"

  cut_count=$(gum input --char-limit=2 --placeholder "1" --header="How many segments would you like?")

  if [ -z "$cut_count" ]; then
    exit 1
  fi

  for counter in {1.."$cut_count"}; do
    # Get start and end time
    start=$(gum input --placeholder="02:25" --char-limit=5 --header="Start")
    end=$(gum input --placeholder="06:50" --char-limit=5 --header="End")

    # Print segment start and end time
    printf "\n%s\n" "Cut ${counter}:  ${start}-${end}"
    
    # Check if empty
    if [ -z "$start" ] || [ -z "$end" ]; then
     printf "%s\n" "$status_time"
     exit 1
    fi

    segment_name="${filename}_cut${counter}.mp4"

    # Cut video
    gum spin --title "Cutting..." -- ffmpeg -ss "00:${start}.0" -to "00:${end}.0" -i "$filename" -c copy "${segment_name}" || exit 1

    printf "%s\n" "size: $(du -h ${segment_name})"
  done

  encrypt=$(gum choose --item.foreground 250 "Encrypt" "Don't encrypt")

  if [ "$encrypt" = "Encrypt" ]; then
    encrypt_cuts
  else
    exit 0
  fi
  
}

encrypt_cuts() {

  # Header
  eval "$t_encrypt"

  files_to_encrypt=$(/usr/bin/ls | grep -vE "$GREP_IGNORE" | fzf "${FZF_OPTIONS[@]}")

  if [ -z "$files_to_encrypt" ]; then
    exit 1
  fi

  while IFS= read -r file; do
    gum spin --title="Encrypting..." -- gpg -r numbat@test.com -e "$file" || exit 1
  done <<< "$files_to_encrypt"


  remove_files=$(gum choose --item.foreground 250 "Remove" "Don't remove")

  if [ "$remove_files" = "Remove" ]; then
    securely_remove_files
  else
    exit 0
  fi
}

decrypt_cuts() {

  # Header
  eval "$t_decrypt"

  # Find all gpg files and remove "./"
  files_to_decrypt=$(find . -type f -name "*gpg*" | grep -v "$GREP_IGNORE" | sed 's/\.\///g' | fzf "${FZF_OPTIONS[@]}")

  if [ -z "$files_to_decrypt" ]; then
    exit 1
  fi

  while IFS= read -r file; do
    output_name=$(echo "$file" | sed 's/.gpg//g')
    gpg -d "$file" > "$output_name" || exit 1
  done <<< "$files_to_decrypt"

}

securely_remove_files() {

  # Header
  eval "$t_remove"

  files_to_remove=$(/usr/bin/ls | grep -vE "$GREP_IGNORE" | fzf "${FZF_OPTIONS[@]}")

  while IFS= read -r file; do
    gum spin --title="Erasing..." -- shred -vun 20 "$file" || exit 1
  done <<< "$files_to_remove"


  printf "%s\n" "$status_removed"
  
}

move_files() {

  # Header
  eval "$t_move"

  files_to_move=$(/usr/bin/ls *.mp4 | grep -vE "$GREP_IGNORE" | grep -i "cut" | fzf "${FZF_OPTIONS[@]}")
  dest="~/Documents/Seafile/Videos"


  while IFS= read -r file; do
    gum spin --title="Moving to another directory..." -- mv "$file" "$dest" || exit 1
  done <<< "$files_to_move"

}

main
