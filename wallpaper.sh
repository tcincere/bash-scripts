#!/usr/bin/env bash

DIR_WALLPAPER="$HOME/Documents/Seafile/Cloud/Photos/Wallpapers"
CONFIG="$HOME/.config/hypr/hyprpaper.conf"


main() {

  # Get base names of all images.
  basenames=$(/usr/bin/fd . "$DIR_WALLPAPER" -d1 -tf -x basename)

  # Get basename of image chosen.
  image_chosen=$(/usr/bin/rofi -dmenu -i <<< "$basenames")

  echo $image_chosen

  full_path=""

  # Check if image chosen is not empty
  if [ -n "$image_chosen" ]; then
    full_path=$(/usr/bin/fd "$image_chosen" "$DIR_WALLPAPER")
  else
    exit 1
  fi

  # Check if absolute path is valid.
  if [ -f "$full_path" ]; then
    echo "works"
  else
    exit 1
  fi

  # Change wallpaper using hyprctl  
  /usr/bin/hyprctl hyprpaper unload all
  /usr/bin/hyprctl hyprpaper preload "$full_path"
  /usr/bin/hyprctl hyprpaper wallpaper "DP-3, $full_path"

  # Modify hyprpaper file for next startup (becomes default)
  /usr/bin/sed -i "/preload/c\preload = $full_path" "$CONFIG"
  /usr/bin/sed -i "/wallpaper/c\wallpaper = ,$full_path" "$CONFIG"

}

# Call function main
main
