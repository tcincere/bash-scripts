#!/bin/bash

fd_location="~/.local/bin/fd"
# MENU="dmenu -i -p Search -fn FragmentMono:size=12 -sf ghostwhite"
MENU="rofi -dmenu -i -p Search"
EDIT="${KITTY_PATH} -e ${HELIX_PATH}"
