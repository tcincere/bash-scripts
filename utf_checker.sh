#!/bin/sh

if ! [ $(command -v iconv) ]; then
    printf "iconv not found! Exiting..."
    exit 1
fi

if [ -z "$1" ]; then
    printf "Input file not given. Exiting..."
    exit
fi


printf "UTF-8 checker"
file="$1"

iconv -f utf-8 "$file" -o /dev/null

if [ $? -eq 0 ]; then
    printf "File is valid UTF-8"
else
    printf "File is NOT valid UTF-8"
fi


