#!/usr/bin/env zsh

# Variables
HISTORY_FILE="$HOME/.config/zsh/.zsh_history"

FZF_OPTIONS=(-i --height=40% \
--pointer='>>'
--prompt="> "
--color=16 --reverse 
--info=hidden 
--color=fg:242,fg+:#ffcb1c,hl:#b16286,hl+:#4cb550,border:#dedddd 
--header="" 
--border=rounded)


# Command which removes unix timestamps from zsh history file.
# Pipes output into sort and fzf with options.
cmd=$(sed 's/^[^;]*;//; s/:[0-9]*://' "$HISTORY_FILE" | sort -u | fzf "${FZF_OPTIONS[@]}")

# Selected command is then outputted to screen
if [ -n "$cmd" ]; then
  # xdotool used for xorg
  # printf "%s" "$cmd" | xdotool type --clearmodifiers --file -
  # wtype "$cmd"
  # Workaround for Gnome on Wayland. Copies text to clipboard, instead of writing it out.
  printf "%s" "$cmd" | /usr/bin/wl-copy -n
  exit 0
else
  # echo "\$cmd is empty!"
  exit 1
fi
