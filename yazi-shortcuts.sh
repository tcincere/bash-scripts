#!/usr/bin/env sh

FILE="$HOME/.config/yazi/keymap.toml"

sed -n '8,$p' "$FILE" | sed -n 's/.*on = "\([^"]*\)".*run = "\([^"]*\)".*desc = "\([^"]*\)".*/ ( \1 ),  \2,  \3/p' | rofi -dmenu -p "Yazi Keybinds 󰇥 " -l 20 >/dev/null &
