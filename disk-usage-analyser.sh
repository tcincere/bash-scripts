#!/usr/bin/env bash

source variables.sh

# Options for fzf
FZF_OPTIONS=(-i -m \
--height=40% 
--pointer='>>'
--prompt="> "
--color=16
--info=hidden 
--color=fg:242,fg+:#ffcb1c,hl:#b16286,hl+:#4cb550,border:#dedddd 
--header="" 
--border=rounded)

# Analyse disk usage; exclude some directories; sort by human-readable number.
chosen_dir=$(du -ah $HOME --max-depth=1 --exclude="Trash" --exclude=".themes" --exclude=".fonts" --exclude=".icons" --exclude=".var"  --exclude=".logseq" --exclude=".cache" --threshold=50M 2>/dev/null | sort -h | fzf "$FZF_OPTIONS" | awk '{print $2}')

files_in_dir=$(du -ah "$chosen_dir" --threshold=50M | sort -h | fzf "$FZF_OPTIONS" | awk '{print $2}')

echo "$files_in_dir"
for file in "${files_in_dir[@]}"; do
  if [ -f "$file" ]; then
    rm -i "$file"
  else
    echo "$file isn't a file. Skipping..."
  fi
done


