#!/usr/bin/env sh
# Filename: debfetch.sh
# Function: Fetch basic system information for Debian based systems
# by: tcincere [https://gitlab.com/tcincere] 

# Colours
esc="$(printf  "\033")"
red="${esc}[1;31m"
yellow="${esc}[1;33m"
purple="${esc}[1:35m"
cyan="${esc}[1;36m"
white="${esc}[1;37m"
reset="${esc}[0m"

# Typical system information
user=$(whoami)
distro=$(awk -F "=" 'NR == 1 {gsub("\"",""); print tolower($2)}' /etc/os-release)
host="$(uname -n)"
kernel="$(uname -r)"
packages=$(dpkg -l | wc -l)
flatpak_packages=$(flatpak list --app  | rev | wc -l)
shell="$(grep "$user" /etc/passwd | cut -d ":" -f 7)"

# Memory
memory_total=$(vmstat -s | head -n 2 | awk '$3 == "total" {print $1; exit(0)}' | numfmt --from-unit=K --to=iec)
memory_use=$(vmstat -s | head -n 2 | awk '$3 == "used" {print $1; exit(0)}' | numfmt --from-unit=K --to=iec)

# Uptime
uptime=$(uptime -p | sed 's/up //')

# Information and header colours.
info_type="${reset}${purple}"
info_info="${reset}${white}"
arrow="${reset}${cyan}->"

# Printing the output

/usr/bin/cat <<EOF

${white} debfetch
${info_type} user      ${arrow}  ${info_info}${user} (@${host}) 
${info_type} distro    ${arrow}  ${info_info}${distro} 
${info_type} kernel    ${arrow}  ${info_info}${kernel} 
${info_type} pkgs      ${arrow}  ${info_info}${packages} (deb), ${flatpak_packages} (flatpak)
${info_type} memory    ${arrow}  ${info_info}${memory_use}/${memory_total} 
${info_type} shell     ${arrow}  ${info_info}${shell}
${info_type} uptime    ${arrow}  ${info_info}${uptime} 

EOF