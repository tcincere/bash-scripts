# Shell scripts
---
### Information

This repository features shell scripts written in POSIX shell, Bash or ZSH that makes Linux more efficient.
The scripts featured are:

- *applauncher.sh*: Launch appimages from directory
- *debfetch.sh*: Neofetch-like script for debian-based systems.
- *dictionary.sh*: Search definitions and synonyms for english words.
- *downloader.sh*: Download, cut, encrypt, decrypt and move videos using `yt-dlp`. Uses `gum` for options chooser. 
- *file-search.sh*: Uses `fzf` to search for files and open them using specific programs.
- *kpurge*: remove old kernels, keeping an extra kernel image as redundancy.
- *pass-gen*: generate random passwords using OpenSSL.
- *rsh*: a reverse shell one liner.
- *sbruteforce*: bruteforce a specified URL using a wordlist and `curl`.
- *screenshot.sh*: Create screenshots using `dmenu` and `grim` (wayland)
- *tr_dl*: Script which uses `translate shell` to download audio to either german or russian.
- *vmstart*: create and configure virtual machines using qemu via `fzf`.
- *wallpaper.sh*: Set wallpaper using `rofi`.
- *xopen*: file search within specified directory to open them using default application with `xdg-open`
- *xrezset*: set a custom resolution using `xrandr` and `cvt`
- *yazi-shortcuts*:  Print shortcuts of `yazi` file manager using `fzf` and `rofi`.
- *zsh_history_search.sh*: Search through zsh history using history file and `fzf`
