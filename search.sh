#!/usr/bin/env bash

# Browser (firefox, librewolf etc)
browser="$1"
query="$2"
engine="$3"
search_query=""
url_encoded_query=""
full_query=""

if [ -n "$browser" ] && [ -n "$query" ]; then
  url_encoded_query=$(python3 -c "import urllib.parse; print(urllib.parse.quote('$query'))")

  case $engine in
    "ddg" | "duckduckgo" | "duck")
      search_query="https://duckduckgo.com/?q="
      ;;
    "br" | "brave")
      search_query="https://search.brave.com/search?q="
      ;;
    "st" | "start" | "startpage")
      search_query="https://www.startpage.com/sp/search?query="
      ;;
    "yt" | "you" | "youtube")
      search_query="https://youtube.com/results?search_query="
      ;;
    *)
      echo "Unknown search engine. Exiting..."
      exit 1
      ;;
  esac

  full_query="$search_query""$url_encoded_query"

else
  echo "Parameters missing. Exiting..."
  exit 1
fi
  
# TODO: Add youtube implemetation
function main() {

  flatpak_id=$(flatpak list | grep -i "$browser" | awk '{print $2}')


  if [ -n "$flatpak_id" ]; then
    flatpak run "$flatpak_id" "$full_query" &>/dev/null &
  elif [ -z "$flatpak_id" ] && [ "$browser" = "brave" ]; then
    brave=$(whereis brave-browser | awk '{print $2}')
    $brave --incognito "$full_query" &>/dev/null &
  else
    echo "Couldn't parse flatpak ID. Exiting..."
  fi
  
}

main
    

