#!/usr/bin/env bash

# Variables
mkdir -p $HOME/Pictures/screenshots
folder="$HOME/Pictures/screenshots"


# Enter name of screenshot
name="$(rofi -dmenu -p "Output" -l 0)"

# Check if name is empty, if so, exit 1
if [ -z "$name" ]; then
  notify-send "Screenshot not taken!" "Output name empty!"
  exit 1
fi


# Cat folder with name
filename="$folder/$name.png"


# Take screenshot
grim -g "$(slurp)" "$filename"


# Check if file exists on system,
# Means screenshot is successful
if [ -f "$filename" ]; then
  notify-send "Screenshot successfully taken!"
else
  notify-send "Screenshot not taken"
fi
