#!/usr/bin/env bash

# Define the directory to search
directory="$HOME"
FD_CMD=""

if command -v fd &> /dev/null; then
    FD_CMD="fd"
elif command -v fdfind &> /dev/null; then
    FD_CMD="fdfind"
fi

# Define FZF options
FZF_OPTIONS=(-i --height=55% \
--pointer='*'
--prompt="> "
--color=fg:242,fg+:#ffcb1c,hl:#b16286,hl+:#4ca3dd,border:#4ca3dd,bg:#1c1c1c,bg+:#7d4e9f
--reverse
--header=""
--border=rounded)

# Define excluded directories in an array
EXCLUDES=(
  ".seafile-data"
  ".icons"
  ".cargo"
  ".rustup"
  ".steam"
  ".var"
  ".lib"
  ".ansible"
  ".dbus"
  "Trash"
  ".cache"
  ".local/share/"
  ".git"
  ".mozilla"
  "cosmic"
  ".themes"
  ".bun"
  ".local/lib"
  "libreoffice"
  "freetube"
)

# Convert the array to a format that fd understands
EXCLUDE_FLAGS=()
for dir in "${EXCLUDES[@]}"; do
  EXCLUDE_FLAGS+=("-E" "$dir")
done

# Run fd with the exclude flags
# Cut at home directory then pipe into fzf
choice=$($FD_CMD -t d -H "${EXCLUDE_FLAGS[@]}" --full-path "" "$directory" | cut -d "/" -f 4- | sort |fzf "${FZF_OPTIONS[@]}")

# Echo directory and add back '/home/...' plus quotes around directory for spaces.
target=$(echo "${directory}/${choice}")

# Check if variable is not empty, then print
[ -n "$target" ] && echo "$target"

# target=$(echo $choice | awk -v dir="$directory/" '{print "\"" dir$0 "\""}')
