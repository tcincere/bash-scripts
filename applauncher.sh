#!/bin/bash

apps_dir="$HOME/Downloads/appimages/"

function select_app_image() {
  app=$(dir "$apps_dir" | xargs -n 1 |  rofi -dmenu)

  if [ "$app" ]; then
    file="$apps_dir$app"

    if [ -x "$file" ]; then
     "$file" &>/dev/null &
    else
      chmod u+x "$file" && "$file" &>/dev/null &
    fi

  fi
}

select_app_image
