#!/bin/bash

# Tesseract language library.
lang=("rus" "eng")


source ~/Documents/git/dmenu-projects/variables.sh
# Customization
# lines="-l 2"
# colour="-sf black -sb plum"
# font="DMMono:Medium:size=12"
# prompt="flamegrabber"

# center_border="-c -bw 2"

# Directory to save images.
img_dir="/tmp/imgs"

# Name generator for image files.
rand_name="$(/usr/bin/openssl rand -hex 5)"

# Create directory; no error if exists.
/usr/bin/mkdir -p $img_dir

#copy=$(/usr/bin/xclip -selection clipboard -r)

function main()
{
    language=$(printf "%s\n" "${lang[@]}" | $MENU ) || exit 1 
    
    if [[ "$language" ]]; then

        # Take screenshot specifying name and directory.
        # /usr/bin/flameshot gui -p "$img_dir/$rand_name".png
        grim -g "$(slurp)" "$img_dir/$rand_name".png

        # Call tesseract and output file with specified language.
        tesseract "$img_dir/$rand_name".png "$img_dir"/text -l $language  && \

        # Cat file contents and save to variable
        contents=$(/usr/bin/cat "$img_dir"/text.txt)
    
        # Echo contents of file; uppercase first letter. Copy to clipboard remove newline (-r) 
        case $language in
                "rus")
                /usr/bin/echo ${contents@u} | wl-copy -n
                ;;
                                
                "eng")
                /usr/bin/echo ${contents} | wl-copy -n 
                ;;
        esac
    else
        exit 1
    fi
}

# Call function
main
