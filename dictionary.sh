#!/usr/bin/env bash

word="$1" 
purpose="$2"
link="https://api.dictionaryapi.dev/api/v2/entries/en_GB/${word}"
header="DEFINITION: ${word}"
header_syn="SYNONYMS: ${word}"
# line="<====================>"
line="---------------------->>"

print_header() {

	if [ "$2" = "def" ]; then
		printf "%s\n%s\n\n" "$header" "$line"
	else
		printf "%s\n%s\n\n" "$header_syn" "$line"
	fi
}

define_word() {
	curl -s "$link" | jq -r '.[0].meanings[].definitions[]' | grep -E "definition|example" | sed '/example/a \ \"' | sed 's/\"definition\"\:/(def)/g' | sed 's/example/\(e.g.\)/g'| tr -d '"' | tr -d ":"
}

get_synonyms() {
	curl -s "$link" | jq -r '.[0].meanings[].synonyms[]'
}
	

check_purpose() {
	if [ -z "$purpose" ]; then 
		printf "%s\n" "[Error] You left out a positional parameter!"
		exit 1
	fi

	# if [ -z "$word" ]; then 
	# 	printf "%s\n" "[Error] You left out the word!"
	# 	exit 1
	# fi
			
	print_header
echo "$1" "$2"
	if [ "$purpose" = "def" ]; then 
		define_word
	fi

	if [ "$purpose" = "syn" ]; then 
		get_synonyms
	fi
}

check_purpose

